## user 12 S1RD32.55-67 25c3ec release-keys
- Manufacturer: motorola
- Platform: lahaina
- Codename: dubai
- Brand: motorola
- Flavor: user
- Release Version: 12
- ID: S1RD32.55-67
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Fingerprint: motorola/dubai_g/dubai:11/S1RD32.55-67/25c3ec:user/release-keys
